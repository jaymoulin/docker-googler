VERSION ?= v4.3.2
CACHE ?= --no-cache=1

.PHONY: all build publish
all: build publish
build:
	docker buildx build --platform linux/arm/v7,linux/arm64/v8,linux/arm/v6,linux/386,linux/amd64 ${PUSH} --build-arg VERSION=${VERSION} --tag jaymoulin/googler --tag jaymoulin/googler:${VERSION} ${CACHE} .
publish:
	PUSH=--push CACHE= make build

