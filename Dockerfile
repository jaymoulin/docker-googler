FROM python:alpine

ARG VERSION=v4.3.2
ARG TARGETPLATFORM
LABEL maintainer="Jay MOULIN <https://jaymoulin.me>"
LABEL version=${VERSION}-${TARGETPLATFORM}

RUN apk update && \
apk add git make --virtual .build-deps && \
git clone https://github.com/jarun/googler.git && \
cd googler && \
git checkout ${VERSION-v3.4} && \
make install && \
cd .. && \
rm -Rf googler && \
apk del git --purge .build-deps

ENV BROWSER=
ENTRYPOINT ["googler"]
